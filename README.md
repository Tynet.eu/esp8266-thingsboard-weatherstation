# ESP8266 MQTT Weatherstation

We use a ESP8266 to repalce the Controller of a Weatherstation to get all Data over WIFI and MQTT to a Thingsboard instance, that shows a nice Dashbaord including Statistics.

The Station / Sensors are the same as Sparkfuns "Weather Meters" SEN-08942, you can find it quite cheap on Aliexpress directly from the Manufacturer.
In Contrest to the Sparkfun Station the OEM one comes with a Temperature Sensor and a little controller that are covered in a sun shielding.

I made a 3D Printable part that fits into the Sun Shield and repalces the original controller. It houses a Wemos D1 mini and a DHT22 Temperature and Humidity Sensor.
The other original Sensors get soldered to the ESP8266 that reads them and sends all Data to a Thingsboard Instance.
If you don't like Thingsboard, you can use any Platform that speaks MQTT, but you need to modify the Code a bit for that.

# What Data you get

* Wind Direction
* Wind Speed
* Rain Amount
* Temperature
* Humidity

# What you need

*  Wemos D1 mini (or a similar ESP8266)
*  DHT22 Sensor
*  MS-WH-SP-WS02 Weather Station (Similar to Sparkfun SEN-08942)
*  3D Printed Part
*  Some basic tools
*  Soldering stuff and a bit of cable
*  Thingsboard Instance (Use Docker for example)


You can find more Info including a Video about the Project on my website: [Tysonpower.de](https://tysonpower.de/blog/esp8266-weather-station)